const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();
const TABLE_NAME = process.env.TABLE_NAME || '';
const PRIMARY_KEY = process.env.PRIMARY_KEY || '';
const UPLOAD_BUCKET = process.env.UPLOAD_BUCKET || '';
const UPLOAD_PATH = process.env.UPLOAD_PATH || '';
import { corsHeaders } from './cors-headers'

// Updateable properties go in this list
const updateDBProperties: [string] = ['eventDate'];

const RESERVED_RESPONSE = `Error: You're using AWS reserved keywords as attributes`,
  DYNAMODB_EXECUTION_ERROR = `Error: Execution update, caused a Dynamodb error, please take a look at your CloudWatch Logs.`;

export const handler = async (event: any = {}) : Promise <any> => {

  if (!event.body) {
    return { statusCode: 400, body: 'invalid request, you are missing the parameter body' };
  }

  const editedItemId = event.pathParameters.id;
  if (!editedItemId) {
    return { statusCode: 400, body: 'invalid request, you are missing the path parameter id' };
  }

  const editedItem: any = typeof event.body == 'object' ? event.body : JSON.parse(event.body);
  if (!(editedItem["eventDate"] || editedItem["image"])) {
    return { statusCode: 400, body: 'invalid request, you must provide either eventDate or image in the request body' };
  }

  // Upload event poster to S3
  if (editedItem["image"]) {
    const encodedImage = JSON.parse(event.body).image;
    const decodedImage = Buffer.from(encodedImage, 'base64');
    const filePath = UPLOAD_PATH + "/" + editedItemId + ".jpg";

    try {
      console.log("Uploading image to S3...");
      const s3Params = {
        Body: decodedImage,
        Bucket: UPLOAD_BUCKET,
        Key: filePath
      };
      const response = await s3.putObject(s3Params).promise();
      console.log("S3 upload successful: " + JSON.stringify(response));
    } catch (s3UploadError) {
      console.log("Error uploading to S3: " + JSON.stringify(s3UploadError));
      return { statusCode: 500, body: 'Error uploading event poster to S3.' };
    }
  }

  let editableProperties = Object.keys(editedItem).filter(x => updateDBProperties.includes(x));
  console.log('editableProperties: ' + JSON.stringify(editableProperties));
  const firstProperty = editableProperties.splice(0,1);
  const params: any = {
    TableName: TABLE_NAME,
    Key: {
      [PRIMARY_KEY]: editedItemId
    },
    UpdateExpression: `set ${firstProperty} = :${firstProperty}`,
    ExpressionAttributeValues: {},
    ReturnValues: 'UPDATED_NEW'
  };
  params.ExpressionAttributeValues[`:${firstProperty}`] = editedItem[`${firstProperty}`];

  editableProperties.forEach(property => {
      params.UpdateExpression += `, ${property} = :${property}`;
      params.ExpressionAttributeValues[`:${property}`] = editedItem[property];
  });
  console.log(JSON.stringify(params));

  try {
    const response = await db.update(params).promise();
    console.log("db response: " + JSON.stringify(response));
    return { statusCode: 204, body: '', headers: corsHeaders };
  } catch (dbError) {
    const errorResponse = dbError.code === 'ValidationException' && dbError.message.includes('reserved keyword') ?
      DYNAMODB_EXECUTION_ERROR : RESERVED_RESPONSE;
    console.log(dbError);
    return { statusCode: 500, body: errorResponse };
  }
};
