#!/usr/bin/env node
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { HssApi, HssApiProps } from '../lib/hss-api';
const devProps: HssApiProps = {
  "stage": "dev",
  "projectName": "hss-api",
  "uploadPath": "hssapi/dev"
};
const stagingProps: HssApiProps = {
  "stage": "staging",
  "projectName": "hss-api",
  "uploadPath": "hssapi/staging"
};
const app = new cdk.App();
new HssApi(app, 'HssApiDev', devProps);
new HssApi(app, 'HssApiStaging', stagingProps);
