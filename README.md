# Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile <- VERY IMPORTANT!!
 * `npm run zip`     generate or update the deployment package zip file
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template

# API Spec
## Endpoints
/events
## Methods And Parameters
Retrieve a specific event:
* **Path**: /events/{id}
* **Method**: GET

Retrieve all events:
* **Path**: /events
* **Method**: GET

Create an event:
* **Path**: /events
* **Method**: POST
* **Parameters, required(request body)**: eventDate, image(encoded)
Example input: 
```json
{
  "eventDate": 1569891600,
  "image": "<binary data>"
}
```

Update an event:
* **Path**: /events/{id}
* **Method**: PATCH
* **Parameters, required(request body)**: eventDate, image(encoded)  
```json
{
  "eventDate": 1569891600,
  "image": "<binary data>"
}
```

Delete an event:
* **Path**: /events/{id}
* **Method**: DELETE


## Prerequisites
1. node.js/npm
1. An AWS account
1. A profile with API credentials configured for the above account

## Configure Development environment and launch infrastructure
1. Set your AWS profile:
`export AWS_PROFILE=<profile_name>`
1. Install packages: `npm i`
1. Update the API props in`bin/hss-api` if you want different values
1. Build deployment artifacts: `npm run build`
1. Generate deployment package: `npm run pack`
1. Deploy stacks: `cdk deploy`
1. Note the stack output showing the root API Endpoint: https://<id>.execute-api.<region>.amazonaws.com/prod

## Finish User Pool setup
These steps will outline how to create a Cognito user for yourself(and other app users).
1. Log into the AWS Web Console
1. Navigate to `Cognito -> App integration -> App client settings`
* note the `App Client ID` at the top, i.e. something like `7u82dapl60kf4vdmb6li0um9s8`
* check `Cognito User Pool` under `Enabled Identity Providers`
* Enter values for `Callback URL(s)`(where the user is redirected after logging in) and `Sign out URL(s)`(where the user is redirected after logging out)
* check `Authorization code grant` under `Allowed OAuth Flows`
* check `openid` under `Allowed OAuth Scopes`
1. Navigate to `Cognito -> App integration -> Domain name`, enter a value for the domain prefix and check that it is available: i.e. `devhssapi`
1. Create it once available

## Create user
1. In the Cognito console, go to `General Settings -> Users and Groups`, then click `Create user` to make a user for yourself in the User Pool
1. Enter your email address under `Username`, check the `Email` box beneath to get sent your temporary password, uncheck `Mark phone number as verified`, and enter your email address again in the bottom `Email` field, then click `Create`

## Log in to change password and start using app
Once you've created your user and received the email with your temporary password, you can visit the Amazon-provided URL by filling in some relevant values. The URL is formatted as follows:

`https://<domain_name>.auth.us-west-2.amazoncognito.com/login?client_id=<client_id>&redirect_uri=https://<redirect_uri>&response_type=code`

* **domain_name**: The domain name you configured previously(i.e. `devhssapi`)
* **client_id**: The App Client ID you noted previously
* **redirect_uri**: The redirect URI you configured previously