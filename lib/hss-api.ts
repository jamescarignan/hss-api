import apigateway = require('@aws-cdk/aws-apigateway');
import dynamodb = require('@aws-cdk/aws-dynamodb');
import lambda = require('@aws-cdk/aws-lambda');
import cognito = require('@aws-cdk/aws-cognito');
import s3 = require('@aws-cdk/aws-s3');
import cdk = require('@aws-cdk/core');
import {AuthFlow, SignInType} from "@aws-cdk/aws-cognito";

export interface HssApiProps extends cdk.StackProps {
  projectName: string;
  stage: string;
  uploadPath?: string;
}

export class HssApi extends cdk.Stack {
  constructor(app: cdk.App, id: string, props: HssApiProps) {
    super(app, id);

    const dynamoTable = new dynamodb.Table(this, 'events', {
      partitionKey: {
        name: 'eventId',
        type: dynamodb.AttributeType.STRING
      },
      tableName: props.projectName + '-' + props.stage + '-events',

      // The default removal policy is RETAIN, which means that cdk destroy will not attempt to delete
      // the new table, and it will remain in your account until manually deleted. By setting the policy to
      // DESTROY, cdk destroy will delete the table (even if it has data in it)
      removalPolicy: cdk.RemovalPolicy.DESTROY, // NOT recommended for production code
    });

    // Upload bucket ref

    const uploadBucket = new s3.Bucket(this, 'uploadBucket');
    uploadBucket.grantPublicAccess();
    const uploadPath = (props.uploadPath === "") ? "" : props.uploadPath!;

    const getOneLambda = new lambda.Function(this, 'getOneEventFunction', {
      code: new lambda.AssetCode('./create.zip'),
      handler: './lib/src/get-one.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: dynamoTable.tableName,
        PRIMARY_KEY: 'eventId',
        UPLOAD_BUCKET: uploadBucket.bucketName,
        UPLOAD_PATH: uploadPath
      }
    });

    const getAllLambda = new lambda.Function(this, 'getAllEventsFunction', {
      code: new lambda.AssetCode('./create.zip'),
      handler: './lib/src/get-all.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: dynamoTable.tableName,
        PRIMARY_KEY: 'eventId',
        UPLOAD_BUCKET: uploadBucket.bucketName,
        UPLOAD_PATH: uploadPath
      }
    });
    const createOne = new lambda.Function(this, 'createEventFunction', {
      code: new lambda.AssetCode('./create.zip'),
      handler: './lib/src/create.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: dynamoTable.tableName,
        PRIMARY_KEY: 'eventId',
        UPLOAD_BUCKET: uploadBucket.bucketName,
        UPLOAD_PATH: uploadPath
      }
    });

    const updateOne = new lambda.Function(this, 'updateEventFunction', {
      code: new lambda.AssetCode('./create.zip'),
      handler: './lib/src/update-one.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: dynamoTable.tableName,
        PRIMARY_KEY: 'eventId',
        UPLOAD_BUCKET: uploadBucket.bucketName,
        UPLOAD_PATH: uploadPath
      }
    });

    const deleteOne = new lambda.Function(this, 'deleteEventFunction', {
      code: new lambda.AssetCode('./create.zip'),
      handler: './lib/src/delete-one.handler',
      runtime: lambda.Runtime.NODEJS_10_X,
      environment: {
        TABLE_NAME: dynamoTable.tableName,
        PRIMARY_KEY: 'eventId',
        UPLOAD_BUCKET: uploadBucket.bucketName,
        UPLOAD_PATH: uploadPath
      }
    });

    dynamoTable.grantReadWriteData(getAllLambda);
    dynamoTable.grantReadWriteData(getOneLambda);
    dynamoTable.grantReadWriteData(createOne);
    dynamoTable.grantReadWriteData(updateOne);
    dynamoTable.grantReadWriteData(deleteOne);
    uploadBucket.grantReadWrite(createOne);
    uploadBucket.grantReadWrite(updateOne);
    uploadBucket.grantReadWrite(deleteOne);

    // API Resources

    const api = new apigateway.RestApi(this, 'eventsApi', {
      restApiName: 'Events Service',
      binaryMediaTypes: [
        "image/*",
        "application/octet-stream"
      ]
    });

    const events = api.root.addResource('events');
    const getAllIntegration = new apigateway.LambdaIntegration(getAllLambda);
    events.addMethod('GET', getAllIntegration);

    const createOneIntegration = new apigateway.LambdaIntegration(createOne, {
      contentHandling: apigateway.ContentHandling.CONVERT_TO_BINARY,
      passthroughBehavior: apigateway.PassthroughBehavior.WHEN_NO_MATCH
    });
    const createMethod = events.addMethod('POST', createOneIntegration);
    addCorsOptions(events);

    const singleEvent = events.addResource('{id}');
    const getOneIntegration = new apigateway.LambdaIntegration(getOneLambda);
    singleEvent.addMethod('GET', getOneIntegration);

    const updateOneIntegration = new apigateway.LambdaIntegration(updateOne);
    const updateMethod = singleEvent.addMethod('PATCH', updateOneIntegration);

    const deleteOneIntegration = new apigateway.LambdaIntegration(deleteOne);
    const deleteMethod = singleEvent.addMethod('DELETE', deleteOneIntegration);
    addCorsOptions(singleEvent);

    // Cognito Resources
    const userPool = new cognito.UserPool(this, 'HssApiUserPool', {
      autoVerifiedAttributes: [cognito.UserPoolAttribute.EMAIL],
      signInType: SignInType.EMAIL,
      userPoolName: props.projectName + '-' + props.stage
    });
    const userPoolClient = new cognito.UserPoolClient(this, 'HssApiUserPoolClient', {
      userPool: userPool,
      userPoolClientName: props.projectName + props.stage + '-client',
      enabledAuthFlows: [AuthFlow.USER_PASSWORD]
    });

    // Create the Cognito Authorizer and add it to the relevant methods
    const authorizer = new apigateway.CfnAuthorizer(this, 'Authorizer', {
      name: `myauthorizer`,
      restApiId: api.restApiId,
      type: 'COGNITO_USER_POOLS',
      identitySource: 'method.request.header.Authorization',
      providerArns: [userPool.userPoolArn],
    });

    // Create
    const createChild = createMethod.node.findChild('Resource') as apigateway.CfnMethod;

    createChild.addPropertyOverride('AuthorizationType', 'COGNITO_USER_POOLS');

    createChild.addPropertyOverride('AuthorizerId', { Ref: authorizer.logicalId });

    // Update
    const updateChild = updateMethod.node.findChild('Resource') as apigateway.CfnMethod;

    updateChild.addPropertyOverride('AuthorizationType', 'COGNITO_USER_POOLS');

    updateChild.addPropertyOverride('AuthorizerId', { Ref: authorizer.logicalId });

    // Delete
    const deleteChild = deleteMethod.node.findChild('Resource') as apigateway.CfnMethod;

    deleteChild.addPropertyOverride('AuthorizationType', 'COGNITO_USER_POOLS');

    deleteChild.addPropertyOverride('AuthorizerId', { Ref: authorizer.logicalId });
  }
}

export function addCorsOptions(apiResource: apigateway.IResource) {
  apiResource.addMethod('OPTIONS', new apigateway.MockIntegration({
    integrationResponses: [{
      statusCode: '200',
      responseParameters: {
        'method.response.header.Access-Control-Allow-Headers': "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent'",
        'method.response.header.Access-Control-Allow-Origin': "'*'",
        'method.response.header.Access-Control-Allow-Credentials': "'false'",
        'method.response.header.Access-Control-Allow-Methods': "'OPTIONS,GET,PUT,PATCH,POST,DELETE'",
      },
    }],
    passthroughBehavior: apigateway.PassthroughBehavior.NEVER,
    requestTemplates: {
      "application/json": "{\"statusCode\": 200}"
    },
  }), {
    methodResponses: [{
      statusCode: '200',
      responseParameters: {
        'method.response.header.Access-Control-Allow-Headers': true,
        'method.response.header.Access-Control-Allow-Methods': true,
        'method.response.header.Access-Control-Allow-Credentials': true,
        'method.response.header.Access-Control-Allow-Origin': true,
      },
    }]
  })
}
