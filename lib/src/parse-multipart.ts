interface LambdaHeaders {
    [key: string]: any;
}

function getValueIgnoringKeyCase(object: LambdaHeaders, key: string) : any {
    const foundKey: string = Object
        .keys(object)
        .find(currentKey => currentKey.toLocaleLowerCase() === key.toLowerCase())!;
    return object[foundKey];
}

function getBoundary(event: any) {
    return getValueIgnoringKeyCase(event.headers, 'Content-Type').split('=')[1];
}

module.exports.parse = (event: any, spotText: boolean) => {
    const boundary = getBoundary(event);
    //console.log(boundary);
    const result: any = {};
    const theArray = event.body
        .split("--" + boundary)
        .filter((item: any) => item !== "");
    theArray
        .forEach((item: any) => {
            if (/filename=".+"/g.test(item)) {
                //console.log("filename found");
                const index = item.match(/name=".+"[;]/g)[0].slice(6, -2);
                const data = item.slice(item.search(/name=".+"/g) + item.match(/name=".+"/g)[0].length + 4, -4);
                result[index] = data;
            } else if (/name=".+"/g.test(item)){
                const index = item.match(/name=".+"[;]/g)[0].slice(6, -2);
                const data = item.slice(item.search(/name=".+"/g) + item.match(/name=".+"/g)[0].length + 4, -4);
                result[index] = data;
            }
        });
    return result;
};
