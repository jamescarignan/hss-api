const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const TABLE_NAME = process.env.TABLE_NAME || '';
const UPLOAD_BUCKET = process.env.UPLOAD_BUCKET || '';
const UPLOAD_PATH = process.env.UPLOAD_PATH || '';
import { corsHeaders } from './cors-headers'

export const handler = async () : Promise <any> => {

  const params = {
    TableName: TABLE_NAME
  };

  try {
    const response = await db.scan(params).promise();
    response.Items.forEach((item: any) => {
      item["imageURL"] = 'https://' + UPLOAD_BUCKET + '.s3.amazonaws.com/' + UPLOAD_PATH + "/" + item['eventId'] + ".jpg";
    });
    return { statusCode: 200, body: JSON.stringify(response.Items), headers: corsHeaders };
  } catch (dbError) {
    console.log(dbError);
    return { statusCode: 500, body: JSON.stringify(dbError)};
  }
};
