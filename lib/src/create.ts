const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();
const uuidv4 = require('uuid/v4');
const TABLE_NAME = process.env.TABLE_NAME || '';
const PRIMARY_KEY = process.env.PRIMARY_KEY || '';
const UPLOAD_BUCKET = process.env.UPLOAD_BUCKET || '';
const UPLOAD_PATH = process.env.UPLOAD_PATH || '';
import { corsHeaders } from './cors-headers'

const RESERVED_RESPONSE = `Error: You're using AWS reserved keywords as attributes`,
  DYNAMODB_EXECUTION_ERROR = `Error: Execution update, caused a Dynamodb error, please take a look at your CloudWatch Logs.`;

export const handler = async (event: any = {}) : Promise <any> => {
  // Parse out the event body, do some basic input checking
  if (!event.body) {
    return { statusCode: 400, body: 'invalid request, you are missing the parameter body' };
  }

  const item = typeof event.body == 'object' ? event.body : JSON.parse(event.body);
  if (!item["eventDate"]) {
    return { statusCode: 400, body: 'Invalid request, your request body is missing parameter: eventDate'}
  }
  if (!item["image"]) {
    return { statusCode: 400, body: 'Invalid request, your request body is missing parameter: image'}
  }
  if (typeof item["eventDate"] != "number") {
    return { statusCode: 400, body: 'Invalid request, parameter "eventDate" must be of type: number'}
  }

  // Upload event poster to S3
  const encodedImage =JSON.parse(event.body).image;
  const decodedImage = Buffer.from(encodedImage, 'base64');
  const eventId: string = uuidv4();
  const filePath = UPLOAD_PATH + "/" + eventId + ".jpg";

  try {
    console.log("Uploading image to S3...");
    const s3Params = {
      Body: decodedImage,
      Bucket: UPLOAD_BUCKET,
      Key: filePath
    };
    const response = await s3.putObject(s3Params).promise();
    console.log("S3 upload successful: " + JSON.stringify(response));
  } catch (s3UploadError) {
    console.log("Error uploading to S3: " + JSON.stringify(s3UploadError));
    return { statusCode: 500, body: 'Error uploading event poster to S3.' };
  }

  // Save event to DynamoDB
  let hss_event = {
    "eventDate": item["eventDate"],
    [PRIMARY_KEY]: eventId
  };
  const dbParams = {
    TableName: TABLE_NAME,
    Item: hss_event
  };
  console.log(dbParams);
  try {
    const response = await db.put(dbParams).promise();
    console.log(JSON.stringify(response));
    hss_event["imageURL"] = 'https://' + UPLOAD_BUCKET + '.s3.amazonaws.com/' + UPLOAD_PATH + "/" + eventId + ".jpg";
    return { statusCode: 201, body: JSON.stringify(hss_event), headers: corsHeaders }; 
  } catch (dbError) {
    const errorResponse = dbError.code === 'ValidationException' && dbError.message.includes('reserved keyword') ?
        DYNAMODB_EXECUTION_ERROR : RESERVED_RESPONSE;
    console.log(dbError);
    return { statusCode: 500, body: errorResponse };
  }

};
