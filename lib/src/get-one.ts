const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const TABLE_NAME = process.env.TABLE_NAME || '';
const PRIMARY_KEY = process.env.PRIMARY_KEY || '';
const UPLOAD_BUCKET = process.env.UPLOAD_BUCKET || '';
const UPLOAD_PATH = process.env.UPLOAD_PATH || '';
import { corsHeaders } from './cors-headers';

const RESERVED_RESPONSE = `Error: You're using AWS reserved keywords as attributes`,
  DYNAMODB_EXECUTION_ERROR = `Error: Execution update, caused a Dynamodb error, please take a look at your CloudWatch Logs.`;

export const handler = async (event: any = {}) : Promise <any> => {

  console.log(event);
  const eventId = event['pathParameters']['id'];
  console.log("eventId: " + eventId);
  const key: any = {};
  key[PRIMARY_KEY] = eventId;
  const params = {
    TableName: TABLE_NAME,
    Key: key
  };
  console.log(JSON.stringify(params));

  try {
    const response = await db.get(params).promise();
    response.Item["imageURL"] = 'https://' + UPLOAD_BUCKET + '.s3.amazonaws.com/' + UPLOAD_PATH + "/" + eventId + ".jpg";
    console.log(JSON.stringify(response));
    return { statusCode: 200, body: JSON.stringify(response.Item), headers: corsHeaders };
  } catch (dbError) {
    const errorResponse = dbError.code === 'ValidationException' && dbError.message.includes('reserved keyword') ?
      DYNAMODB_EXECUTION_ERROR : RESERVED_RESPONSE;
    console.log(dbError);
    return { statusCode: 500, body: errorResponse };
  }
};
