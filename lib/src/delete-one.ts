const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();
const TABLE_NAME = process.env.TABLE_NAME || '';
const PRIMARY_KEY = process.env.PRIMARY_KEY || '';
const UPLOAD_BUCKET = process.env.UPLOAD_BUCKET || '';
const UPLOAD_PATH = process.env.UPLOAD_PATH || '';
import { corsHeaders } from './cors-headers'

export const handler = async (event: any = {}) : Promise <any> => {

  const requestedItemId = event.pathParameters.id.replace('%20', '');
  if (!requestedItemId) {
    return { statusCode: 400, body: `Error: You are missing the path parameter id` };
  }
  const filePath = UPLOAD_PATH + "/" + requestedItemId + ".jpg";

  // Delete item from S3
  try {
    console.log("Deleting image from S3...");
    const s3Params = {
      Bucket: UPLOAD_BUCKET,
      Key: filePath
    };
    const response = await s3.deleteObject(s3Params).promise();
    console.log("S3 delete successful: " + JSON.stringify(response));
  } catch (s3UploadError) {
    console.log("Error uploading to S3: " + JSON.stringify(s3UploadError));
    return { statusCode: 500, body: 'Error uploading event poster to S3.' };
  }

  const params: any = {
    TableName: TABLE_NAME,
    Key: {
      [PRIMARY_KEY]: requestedItemId
    }
  };
  console.log(JSON.stringify(params));

  try {
    const successMessage = {
      'message': 'item successfully deleted'
    };
    await db.delete(params).promise();
    return { statusCode: 200, body: JSON.stringify(successMessage), headers: corsHeaders };
  } catch (dbError) {
    console.log(dbError);
    return { statusCode: 500, body: JSON.stringify(dbError) };
  }
};
